import Vue from 'vue'; // library
import App from './App.vue'; // principal component
import router from './router'; // router
import store from './store'; // vuex

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
