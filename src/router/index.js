import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Contacto from '../views/Contacto.vue';
import ServiciosBaaS from '../views/Servicios/ServiciosBaaS.vue';
import ServiciosDRaaS from '../views/Servicios/ServiciosDRaaS.vue';
import Talento from '../views/Talento.vue';
import Soporte from '../views/Soporte.vue';
import Portales from '../views/Portales.vue';
import FreeTrial from '../views/FreeTrial.vue';
import Partners from '../views/Partners.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/contacto',
    name: 'Contacto',
    component: Contacto,
  },
  {
    path: '/servicios/baas',
    name: 'ServiciosBaaS',
    component: ServiciosBaaS,
  },
  {
    path: '/servicios/draas',
    name: 'ServiciosDRaaS',
    component: ServiciosDRaaS,
  },
  {
    path: '/talento',
    name: 'Talento',
    component: Talento,
  },
  {
    path: '/soporte',
    name: 'Soporte',
    component: Soporte,
  },
  {
    path: '/portales',
    name: 'Portales',
    component: Portales,
  },
  {
    path: '/freetrial',
    name: 'FreeTrial',
    component: FreeTrial,
  },
  {
    path: '/partners',
    name: 'Partners',
    component: Partners,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
